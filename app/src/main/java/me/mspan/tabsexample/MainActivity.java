package me.mspan.tabsexample;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.google.android.material.tabs.TabLayout;

public class MainActivity extends AppCompatActivity {

    private TabLayout tabLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        tabLayout = findViewById(R.id.tabs);
        ((TextView) tabLayout.getTabAt(0).getCustomView().findViewById(R.id.tabTextView)).setText(R.string.tab1);
        ((TextView) tabLayout.getTabAt(1).getCustomView().findViewById(R.id.tabTextView)).setText(R.string.tab2);
        ((TextView) tabLayout.getTabAt(2).getCustomView().findViewById(R.id.tabTextView)).setText(R.string.tab3);
        ((TextView) tabLayout.getTabAt(3).getCustomView().findViewById(R.id.tabTextView)).setText(R.string.tab4);

        tabLayout.addOnTabSelectedListener(new TabLayout.BaseOnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                for (int i = 0; i < tabLayout.getTabCount(); i++) {
                    makeTabNotSelectedAtPosition(i);
                }
                makeTabSelectedAtPosition(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
            }
        });
        for (int i = 0; i < tabLayout.getTabCount(); i++) {
            makeTabNotSelectedAtPosition(i);
        }
        makeTabSelectedAtPosition(0);
    }

    private void makeTabSelectedAtPosition(int position) {
        ((TextView) tabLayout.getTabAt(position).getCustomView().findViewById(R.id.tabTextView)).setTextColor(ContextCompat.getColor(this, R.color.white));
    }

    private void makeTabNotSelectedAtPosition(int position) {
        ((TextView) tabLayout.getTabAt(position).getCustomView().findViewById(R.id.tabTextView)).setTextColor(ContextCompat.getColor(this, R.color.unselectedTab));
    }
}
